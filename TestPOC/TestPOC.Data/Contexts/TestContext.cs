﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using TestPOC.Data.Models;

namespace TestPOC.Data.Contexts
{
    public class TestContext : DbContext
    {
        private string _connectionString;
        public DbSet<Log> Logs { get; set; }

        public DbSet<Employee> Employees { get; set; }
        public TestContext()
        { }
        public TestContext(DbContextOptions<TestContext> dbContextOptions)
            : base(dbContextOptions)
        {
            var sqlServerOptions = dbContextOptions.FindExtension<SqlServerOptionsExtension>();
            if (sqlServerOptions != null)
            {
                this._connectionString = sqlServerOptions.ConnectionString;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

    }
}
