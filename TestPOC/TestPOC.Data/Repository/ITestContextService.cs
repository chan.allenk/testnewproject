﻿using System;
using System.Collections.Generic;
using System.Text;
using TestPOC.Common.Data.Services.Interfaces;
using TestPOC.Data.Contexts;

namespace TestPOC.Data.Repository
{
    public interface ITestContextService : IContext<TestContext>
    { }
}
