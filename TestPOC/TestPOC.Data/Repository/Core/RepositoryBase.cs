﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestPOC.Data.Repository.Core
{
    public class RepositoryServiceBase<TDbContext>
        where TDbContext : DbContext
    {
        public DbSet<TEntity> GetData<TEntity>(TDbContext context) where TEntity : class
        {
            foreach (PropertyInfo property in context.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(DbSet<TEntity>))
                {
                    return property.GetValue(context) as DbSet<TEntity>;
                }
            }
            return null;
        }

        public bool Insert<TEntity>(TDbContext context, TEntity record) where TEntity : class
        {
            foreach (PropertyInfo property in context.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(DbSet<TEntity>))
                {
                    (property.GetValue(context) as DbSet<TEntity>).Add(record);
                    return true;
                }
            }
            return false;
        }

        public bool Delete<TEntity>(TDbContext context, TEntity record) where TEntity : class
        {
            foreach (PropertyInfo property in context.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(DbSet<TEntity>))
                {
                    (property.GetValue(context) as DbSet<TEntity>).Remove(record);
                    return true;
                }
            }
            return false;
        }

        public bool Update<TEntity>(TDbContext context, TEntity record) where TEntity : class
        {
            foreach (PropertyInfo property in context.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(DbSet<TEntity>))
                {
                    return UpdateEntity(context, property.GetValue(context) as DbSet<TEntity>, record);
                }
            }
            return false;
        }

        public bool Patch<TEntity>(TDbContext context, TEntity updatedRecord, params string[] propertyNames)
            where TEntity : class
        {
            bool updated = false;
            var entry = context.Entry(updatedRecord);

            foreach (var name in propertyNames)
            {
                entry.Property(name).IsModified = true;
                updated = true;
            }

            return updated;
        }


        public bool SaveAll(TDbContext context)
        {
            return context.SaveChanges() > 0;
        }

        public async Task<bool> SaveAllAsync(TDbContext context)
        {
            return (await context.SaveChangesAsync()) > 0;
        }

        bool UpdateEntity<T>(TDbContext context, DbSet<T> dbSet, T entity) where T : class
        {
            try
            {
                dbSet.AttachAsModified(entity, context);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public static class RepositoryExtensions
    {
        public static void AttachAsModified<T>(this DbSet<T> dbSet, T entity, DbContext ctx) where T : class
        {
            EntityEntry<T> entityEntry = ctx.Entry(entity);
            if (entityEntry.State == EntityState.Detached)
            {
                // attach the entity
                dbSet.Attach(entity);
            }

            // transition the entity to the modified state
            entityEntry.State = EntityState.Modified;
        }
    }

}
