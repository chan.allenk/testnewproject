﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace TestPOC.Data.Repository.Core
{
    public abstract class ContextServiceBase<TDbContext>
            where TDbContext : DbContext
    {
        public abstract TDbContext GetNewContext();

        public void Execute(Action<TDbContext> p)
        {
            using (TDbContext context = this.GetNewContext())
            {
                p.Invoke(context);
            }
        }

        public TResult Execute<TResult>(Func<TDbContext, TResult> p)
        {
            TResult result = default(TResult);

            using (TDbContext context = this.GetNewContext())
            {
                result = p.Invoke(context);
            }

            return result;
        }

        public async Task ExecuteAsync(Func<TDbContext, Task> p)
        {
            using (TDbContext context = this.GetNewContext())
            {
                await p.Invoke(context);
            }
        }

        public async Task<TResult> ExecuteAsync<TResult>(Func<TDbContext, Task<TResult>> p)
        {
            TResult result = default(TResult);
            using (TDbContext context = this.GetNewContext())
            {
                result = await p.Invoke(context);
            }
            return result;
        }

    }
}
