﻿using System;
using System.Collections.Generic;
using System.Text;
using TestPOC.Data.Contexts;
using TestPOC.Data.Repository.Core;

namespace TestPOC.Data.Repository
{
    public class TestRepositoryService : RepositoryServiceBase<TestContext>, ITestRepositoryService
    { }
}
