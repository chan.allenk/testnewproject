﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestPOC.Data.Contexts;
using TestPOC.Data.Repository.Core;

namespace TestPOC.Data.Repository
{
    public class TestContextService : ContextServiceBase<TestContext>, ITestContextService
    {
        public DbContextOptions<TestContext> Options { get; set; }

        public TestContextService(DbContextOptions<TestContext> options)
        {
            this.Options = options;
        }

        public override TestContext GetNewContext()
        {
            TestContext context = new TestContext(Options);
            context.ChangeTracker.AutoDetectChangesEnabled = true;
            context.ChangeTracker.LazyLoadingEnabled = false;
            return context;
        }
    }
}
