﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestPOC.Common.Data.Services.Interfaces;
using TestPOC.Data.Contexts;

namespace TestPOC.Data.Repository
{
    public interface ITestRepositoryService : IRepository<TestContext>
    {
        DbSet<TEntity> GetData<TEntity>(TestContext context) where TEntity : class;
    }
}
