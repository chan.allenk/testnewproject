﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestPOC.Data.Models
{
    public class Log
    {
        [Key]
        public long Id { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }
    }
}
