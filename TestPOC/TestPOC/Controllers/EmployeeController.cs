﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestPOC.Data.Models;
using TestPOC.Data.Repository;

namespace TestPOC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        ITestContextService TestContextService { get; set; }
        ITestRepositoryService TestRepositoryService { get; set; }
        public EmployeeController(ITestRepositoryService testRepositoryService, ITestContextService testContextService)
        {
            TestRepositoryService = testRepositoryService;
            TestContextService = testContextService;
        }

        [HttpPost("Add")]
        public async Task<long> AddEmployee(string firstname, string lastName, string title, string org)
        {
            long addedId = 0;

            await TestContextService.ExecuteAsync(async (context) =>
            {
                var employee = new Employee()
                {
                    FirstName = firstname,
                    LastName = lastName,
                    Title = title,
                    Organization = org
                };
                TestRepositoryService.Insert(context, employee);

                await TestRepositoryService.SaveAllAsync(context);

                addedId = employee.Id;
            });

            return addedId;
        }

        [HttpGet("Get")]
        public async Task<Employee> GetEmployee(long id)
        {
            return await TestContextService.ExecuteAsync(async (context) =>
            {
                return await context.Employees.FirstOrDefaultAsync(p => p.Id == id);
            });
        }
    }
}
