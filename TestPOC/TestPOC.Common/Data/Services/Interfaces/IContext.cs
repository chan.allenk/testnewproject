﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestPOC.Common.Data.Services.Interfaces
{
    public interface IContext<TDbContext>
    {
        TDbContext GetNewContext();

        void Execute(Action<TDbContext> p);

        TResult Execute<TResult>(Func<TDbContext, TResult> p);

        Task ExecuteAsync(Func<TDbContext, Task> p);

        Task<TResult> ExecuteAsync<TResult>(Func<TDbContext, Task<TResult>> p);
    }
}
