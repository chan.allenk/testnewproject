﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestPOC.Common.Data.Services.Interfaces
{
    public interface IRepository<TDbContext>
    {
        bool Insert<TEntity>(TDbContext context, TEntity record) where TEntity : class;

        bool Delete<TEntity>(TDbContext context, TEntity record) where TEntity : class;

        bool Update<TEntity>(TDbContext context, TEntity record) where TEntity : class;

        bool Patch<TEntity>(TDbContext context, TEntity updatedRecord, params string[] propertyNames)
            where TEntity : class;

        bool SaveAll(TDbContext context);

        Task<bool> SaveAllAsync(TDbContext context);
    }
}
